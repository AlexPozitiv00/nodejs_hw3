/** Class creates an error with 400 status and some message */
class BadRequestError extends Error {
  /**
   * @param {string} message
   */
  constructor(message = 'Bad request!') {
    super(message);
    this.statusCode = 400;
  }
}

module.exports = {
  BadRequestError,
};
