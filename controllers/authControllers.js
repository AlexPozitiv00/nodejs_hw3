const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const generator = require('generate-password');
const sgMail = require('@sendgrid/mail');

const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');


const signUp = async (req, res, next) => {
  const {email, password, role} = req.body;
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });
  await user.save();

  res.status(200).json({message: `User created successfully`});
};

const signIn = async (req, res, next) => {
  const {email, password} = req.body;
  const user = await User.findOne({email: email});

  if (!user) {
    return res.status(400).json({message: `No user ${email} was found`});
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const token = jwt.sign({
    _id: user._id,
    email: user.email,
    role: user.role,
    created_date: user.created_date,
  }, JWT_SECRET);
  res.status(200).json({jwt_token: token});
};

const resetPassword = async (req, res, next) => {
  const newPassword = generator.generate({
    length: 10,
    numbers: true,
  });
  const email = req.body.email;

  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({message: `No user ${email} was found`});
  }

  await User.updateOne(user, {
    $set: {
      password: await bcrypt.hash(newPassword, 10),
    },
  });

  sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  const msg = {
    to: `${email}`,
    from: 'artem13@gmail.com',
    subject: 'Password change',
    text: `Your password was changed! New password is ${newPassword}`,
  };
  await sgMail.send(msg);

  res.status(200).json({message: 'Password was successfully changed.' +
        ' New password was sent to your email'});
};

module.exports = {
  signUp,
  signIn,
  resetPassword,
};
