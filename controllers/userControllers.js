const {User} = require('../models/userModel');
const {BadRequestError} = require('../errors');
const bcrypt = require('bcrypt');

const getUserProfileInfo = async (req, res, next) => {
  res.status(200).json({
    user: {
      _id: req.user._id,
      email: req.user.email,
      created_date: req.user.created_date,
    },
  });
};

const deleteUser = async (req, res, next) => {
  await User.deleteOne(req.user);

  res.status(200).json({message: 'Profile deleted successfully'});
};

const changeUserPassword = async (req, res, next) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findOne(req.user);

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new BadRequestError('Old password is not correct');
  }

  if (oldPassword === newPassword) {
    throw new BadRequestError('Passwords should not be same');
  }

  await User.updateOne(user, {
    $set: {
      password: await bcrypt.hash(newPassword, 10),
    },
  });

  res.status(200).json({message: 'Password changed successfully'});
};

module.exports = {
  getUserProfileInfo,
  deleteUser,
  changeUserPassword,
};
