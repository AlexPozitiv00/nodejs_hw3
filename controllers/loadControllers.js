const {BadRequestError} = require('../errors');
const {Load} = require('../models/loadModel');
const {Log} = require('../models/logModel');
const {Truck} = require('../models/truckModel');
const {loadStateTransitions} = require('../helpers/loadStateTransitions');

const addLoad = async (req, res, next) => {
  const createdBy = req.user._id;
  const {
    name,
    payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
  } = req.body;
  const log = new Log({message: 'Load was created'});
  await log.save();

  const load = new Load({
    created_by: createdBy,
    name,
    payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
    logs: [log],
  });

  await load.save();
  res.status(200).json({message: 'Load was created successfully'});
};

const getLoads = async (req, res, next) => {
  let loads;
  const {offset, limit, status} = req.query;
  const requestOptions = {
    offset: parseInt(offset),
    limit: parseInt(limit),
  };
  if (req.user.role === 'SHIPPER') {
    loads = await Load.find(
        {created_by: req.user._id},
        {__v: 0},
        requestOptions);
  } else if (req.user.role === 'DRIVER') {
    loads = await Load.find(
        {assigned_to: req.user._id},
        {__v: 0},
        requestOptions);
  }
  if (status) {
    loads = loads.filter((load) => load.status === status);
  }
  res.status(200).json({loads});
};

const getActiveLoad = async (req, res, next) => {
  res.status(200).json({load: req.activeLoad});
};

const iterateActiveLoadToNextState = async (req, res) => {
  const currentState = req.activeLoad.state;
  const currentStateInd = loadStateTransitions.indexOf(currentState);
  const newState = loadStateTransitions[currentStateInd + 1];


  const log = new Log({message: `Load state was changed to ${newState}`});
  await log.save();

  await Load.updateOne(req.activeLoad, {
    $set: {state: newState},
    $push: {logs: log},
  });


  if (newState === loadStateTransitions[loadStateTransitions.length - 1]) {
    const log = new Log({
      message: `Load status was changed to SHIPPED`,
    });
    await log.save();
    await Load.updateOne({_id: req.activeLoad.id}, {
      $set: {status: 'SHIPPED'},
      $push: {logs: log},
    });
    await Truck.updateOne({assigned_to: req.user._id}, {$set: {status: 'IS'}});
  }

  res.status(200).json({message: `Load state changed to '${newState}'`});
};

const getLoadById = async (req, res, next) => {
  res.status(200).json({load: req.load});
};

const updateLoadById = async (req, res, next) => {
  if (req.load.status !== 'NEW') {
    throw new BadRequestError('There is only loads' +
      ' with "NEW" status can be updated');
  }

  await Load.updateOne(req.load, req.body);
  res.status(200).json({message: 'Your load was updated successfully'});
};

const deleteLoadById = async (req, res, next) => {
  if (req.load.status !== 'NEW') {
    throw new BadRequestError('There is only loads' +
      ' with "NEW" status can be deleted');
  }

  await Load.deleteOne(req.note);
  res.status(200).json({message: 'Load was deleted successfully'});
};

const postLoadById = async (req, res, next) => {
  if (req.load.status !== 'NEW') {
    throw new BadRequestError('Only NEW loads can be posted');
  }
  const log = new Log({
    message: `Load status was changed to POSTED`,
  });
  await log.save();
  await Load.findOneAndUpdate({_id: req.load.id}, {
    $set: {status: 'POSTED'},
    $push: {logs: log},
  });

  const loadPayload = req.load.payload;
  const {
    length: loadLength,
    width: loadWidth,
    height: loadHeight,
  } = req.load.dimensions;


  const availableTruck = await Truck.findOne({
    status: {$eq: 'IS'},
    assigned_to: {$ne: null},
    payload: {$gt: loadPayload},
    length: {$gt: loadLength},
    width: {$gt: loadWidth},
    height: {$gt: loadHeight},
  });

  if (!availableTruck) {
    const log = new Log({
      message: `Truck wasn't found. Load status was changed to NEW`,
    });
    await log.save();
    await Load.updateOne({_id: req.load.id}, {
      $set: {status: 'NEW'},
      $push: {logs: log},
    });
    throw new BadRequestError('No available Trucks' +
      ' were found. Try again later');
  }
  const newLog = new Log({
    message: `Truck was found. Load assigned for driver
     ${availableTruck.assigned_to}`,
  });
  await newLog.save();
  await Load.updateOne({_id: req.load.id}, {
    $set: {
      status: 'ASSIGNED',
      assigned_to: availableTruck.assigned_to,
      state: loadStateTransitions[0],
    },
    $push: {logs: newLog},
  });
  await Truck.updateOne(availableTruck, {$set: {status: 'OL'}});
  res.status(200).json({
    message: 'Load posted successfully',
    driver_found: true,
  });
};

const getLoadShippingInfoById = async (req, res) => {
  if (!req.load.assigned_to) {
    return res.status(200).json({load: req.load});
  }
  const truck = await Truck.findOne({assigned_to: req.load.assigned_to});
  res.status(200).json({load: req.load, truck: truck});
};

module.exports = {
  addLoad,
  getLoads,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getActiveLoad,
  iterateActiveLoadToNextState,
  getLoadShippingInfoById,
};
