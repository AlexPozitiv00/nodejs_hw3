const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema({
  created_by: {
    type: String,
    require: true,
  },
  name: {
    type: String,
    require: true,
  },
  payload: {
    type: Number,
    require: true,
  },
  pickup_address: {
    type: String,
    require: true,
  },
  delivery_address: {
    type: String,
    require: true,
  },
  dimensions: {
    type: Object,
    required: true,
    width: {
      type: Number,
      require: true,
    },
    length: {
      type: Number,
      require: true,
    },
    height: {
      type: Number,
      require: true,
    },
  },
  assigned_to: {
    type: String,
    require: true,
    default: null,
  },
  status: {
    type: String,
    require: true,
    default: `NEW`,
  },
  state: {
    type: String,
    require: true,
    default: null,
  },
  logs: {
    type: Array,
    required: true,
    default: [],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Load = mongoose.model('Load', loadSchema);
