const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    require: true,
  },
  assigned_to: {
    type: String,
    require: true,
    default: null,
  },
  type: {
    type: String,
    require: true,
  },
  status: {
    type: String,
    require: true,
    default: 'IS',
  },
  width: {
    type: Number,
    require: true,
  },
  length: {
    type: Number,
    require: true,
  },
  height: {
    type: Number,
    require: true,
  },
  payload: {
    type: Number,
    require: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Truck = mongoose.model('Truck', truckSchema);
