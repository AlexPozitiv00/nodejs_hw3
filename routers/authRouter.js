const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./asyncWrapper');
const {
  validateSingUp,
  validateSingIn,
  validatePasswordReset,
} = require('./middlewares/validateAuthMiddleware');
const {
  signUp,
  signIn,
  resetPassword} = require('../controllers/authControllers');

router.post('/register',
    asyncWrapper(validateSingUp),
    asyncWrapper(signUp));

router.post('/login',
    asyncWrapper(validateSingIn),
    asyncWrapper(signIn));

router.post('/forgot_password',
    asyncWrapper(validatePasswordReset),
    asyncWrapper(resetPassword));

module.exports = router;
