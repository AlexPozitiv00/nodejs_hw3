const {BadRequestError} = require('../../errors');

const checkIfUserIsDriver = async (req, res, next) => {
  if (req.user.role === 'DRIVER') {
    next();
  } else {
    throw new BadRequestError('Only drivers can watch this page');
  }
};

const checkIfUserIsShipper = async (req, res, next) => {
  if (req.user.role === 'SHIPPER') {
    next();
  } else {
    throw new BadRequestError('Only shippers can watch this page');
  }
};

module.exports = {
  checkIfUserIsDriver,
  checkIfUserIsShipper,
};
