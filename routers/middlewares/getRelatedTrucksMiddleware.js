const {BadRequestError} = require('../../errors');
const {Truck} = require('../../models/truckModel');

module.exports.getRelatedTrucks = async (req, res, next) => {
  const userId = req.user._id;
  const _id = req.params['id'];


  if (!_id.match(/^[0-9a-fA-F]{24}$/)) {
    throw new BadRequestError('You dont have truck with such id');
  }

  const truck = await Truck.findOne({created_by: userId, _id: _id}, {__v: 0});

  if (!truck) {
    throw new BadRequestError('You dont have truck with such id');
  }

  req.truck = truck;
  next();
};
