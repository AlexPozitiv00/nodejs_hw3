const {BadRequestError} = require('../../errors');
const {Load} = require('../../models/loadModel');

module.exports.getRelatedActiveLoad = async (req, res, next) => {
  const activeLoad = await Load.findOne({
    assigned_to: req.user._id,
    status: {$in: ['ASSIGNED']},
  });
  if (!activeLoad) {
    throw new BadRequestError('You dont have active load now!');
  }
  req.activeLoad = activeLoad;

  next();
};
