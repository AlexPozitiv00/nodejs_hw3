const {BadRequestError} = require('../../errors');
const {Load} = require('../../models/loadModel');

module.exports.checkIfUserIsFree = async (req, res, next) => {
  const activeLoad = await Load.findOne({
    assigned_to: req.user._id,
    status: 'ASSIGNED',
  });

  if (activeLoad) {
    throw new BadRequestError('You are not allowed' +
      ' to do this, while assigned to the load ');
  }
  next();
};
