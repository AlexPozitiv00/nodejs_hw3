const Joi = require('joi');
const {BadRequestError} = require('../../errors');

module.exports.validatePasswordChange = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .required(),

    newPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .required(),

  });

  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    throw new BadRequestError(err.message);
  }

  next();
};
