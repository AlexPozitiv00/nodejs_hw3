const Joi = require('joi');
const {BadRequestError} = require('../../errors');

module.exports.validateLoad = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string()
        .required(),
    payload: Joi.number()
        .required(),
    pickup_address: Joi.string()
        .required(),
    delivery_address: Joi.string()
        .required(),
    dimensions: {
      width: Joi.number()
          .required(),
      length: Joi.number()
          .required(),
      height: Joi.number()
          .required(),
    },
  });
  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    throw new BadRequestError(err.message);
  }

  next();
};
