const Joi = require('joi');
const jwt = require('jsonwebtoken');
const {BadRequestError} = require('../../errors');
const {JWT_SECRET} = require('../../config');
const {User} = require('../../models/userModel');

module.exports.validateToken = async (req, res, next) => {
  const schema = Joi.string();

  if (!req.headers['authorization']) {
    throw new BadRequestError('No authorization token provided!');
  }

  const [tokenType, token] = req.headers['authorization'].split(' ');

  try {
    await schema.validateAsync(tokenType);
    await schema.validateAsync(token);
    const tokenVerification = jwt.verify(token, JWT_SECRET);
    const userWithToken = await User.findOne(
        {_id: tokenVerification._id},
        {__v: 0});

    if (userWithToken) {
      delete tokenVerification['iat'];
      req.user = tokenVerification;
    } else {
      throw new Error();
    }
  } catch (err) {
    throw new BadRequestError('No correct authorization token ' +
        'or token type provided!');
  }

  next();
};
