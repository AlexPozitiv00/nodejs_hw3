const Joi = require('joi');
const {BadRequestError} = require('../../errors');

const validateSingUp = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .required(),

    role: Joi.string()
        .pattern(new RegExp('DRIVER|SHIPPER'))
        .required(),
  });
  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
  next();
};

const validateSingIn = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .required(),

  });
  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
  next();
};

const validatePasswordReset = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
  });
  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
  next();
};

module.exports = {
  validateSingUp,
  validateSingIn,
  validatePasswordReset,
};
