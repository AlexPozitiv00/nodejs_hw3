const Joi = require('joi');
const {BadRequestError} = require('../../errors');

module.exports.validateTruck = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
        .pattern(new RegExp('SPRINTER|SMALL STRAIGHT|LARGE STRAIGHT'))
        .required(),
  });
  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    throw new BadRequestError(err.message);
  }

  next();
};
