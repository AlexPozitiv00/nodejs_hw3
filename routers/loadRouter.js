const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./asyncWrapper');
const {validateToken} = require('./middlewares/validateTokenMiddleware');
const {validateLoad} = require('./middlewares/validateLoadMiddleware');
const {getRelatedLoad} = require('./middlewares/getRelatedLoadMiddleware');
const {
  getRelatedActiveLoad,
} = require('./middlewares/getRelatedActiveLoadMiddleware');

const {
  checkIfUserIsDriver,
  checkIfUserIsShipper,
} = require('./middlewares/checkUserRoleMiddleware');

const {
  addLoad,
  getLoads,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getActiveLoad,
  iterateActiveLoadToNextState,
  getLoadShippingInfoById,
} = require('../controllers/loadControllers');

router.get('/',
    asyncWrapper(validateToken),
    asyncWrapper(getLoads));

router.get('/active',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsDriver),
    asyncWrapper(getRelatedActiveLoad),
    asyncWrapper(getActiveLoad));

router.patch('/active/state',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsDriver),
    asyncWrapper(getRelatedActiveLoad),
    asyncWrapper(iterateActiveLoadToNextState));

router.post('/',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsShipper),
    asyncWrapper(validateLoad),
    asyncWrapper(addLoad));

router.get('/:id',
    asyncWrapper(validateToken),
    asyncWrapper(getRelatedLoad),
    asyncWrapper(getLoadById));

router.put('/:id',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsShipper),
    asyncWrapper(getRelatedLoad),
    asyncWrapper(validateLoad),
    asyncWrapper(updateLoadById));

router.delete('/:id',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsShipper),
    asyncWrapper(getRelatedLoad),
    asyncWrapper(deleteLoadById));

router.post('/:id/post',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsShipper),
    asyncWrapper(getRelatedLoad),
    asyncWrapper(postLoadById));

router.get('/:id/shipping_info',
    asyncWrapper(validateToken),
    asyncWrapper(checkIfUserIsShipper),
    asyncWrapper(getRelatedLoad),
    asyncWrapper(getLoadShippingInfoById));

module.exports = router;
